﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace WinFormsHelpers
{
    public class CustomForm : Form
    {
        public readonly CustomBindingSource BindingSrc;
        public ErrorProvider ErrorProvider;
        private IContainer components;

        public CustomForm()
        {
            InitializeComponent();
            BindingSrc = new CustomBindingSource(components);
        }

        private void CustomForm_Load(object sender, EventArgs e)
        {
            if (this.IsInDesignMode())
                return;

            this.SetFocusEvents();
            KeyPreview = true;
            KeyDown += OnKeyDown;

            BindingSrc.CurrentChanged += BindingSrcOnCurrentChanged;
            BindingSrc.CarregarDados = CarregaDados;
            BindingSrc.BeforeUpdate += BindingSrcOnBeforeUpdate;
            BindingSrc.AfterUpdate += BindingSrcOnAfterUpdate;
            Status = StatusFormulario.Navegando;
        }

        private void BindingSrcOnBeforeUpdate(object sender, object o)
        {
            OnBeforeRefresh(o);
        }

        private void BindingSrcOnAfterUpdate(object sender, object o)
        {
            OnAfterRefresh(o);
        }

        protected virtual void OnBeforeRefresh(object o)
        {

        }

        protected virtual void OnAfterRefresh(object o)
        {

        }

        public enum StatusFormulario
        {
            Initial,
            Navegando,
            Inserindo,
            Editando
        }

        private StatusFormulario _status;
        public StatusFormulario Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (this.IsInDesignMode())
                    return;

                if (_status == value) return;

                _status = value;
                OnStatusChanged();
            }
        }

        protected virtual void OnStatusChanged()
        {

        }

        private void BindingSrcOnCurrentChanged(object sender, EventArgs eventArgs)
        {
            OnCurrentItemChanged();
        }

        protected virtual void OnCurrentItemChanged()
        {
        }

        protected virtual void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.KeyCode)
            {
                case Keys.Enter:
                    {
                        SelectNextControl(ActiveControl, true, true, true, true);
                        keyEventArgs.Handled = true;
                        break;
                    }
                case Keys.Escape:
                    {
                        Close();
                        keyEventArgs.Handled = true;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

        }

        protected override void Dispose(bool disposing)
        {
            KeyDown -= OnKeyDown;
            BindingSrc.CurrentChanged -= BindingSrcOnCurrentChanged;
            BindingSrc.BeforeUpdate -= BindingSrcOnBeforeUpdate;
            BindingSrc.AfterUpdate -= BindingSrcOnAfterUpdate;

            base.Dispose(disposing);
        }

        protected virtual IBindingList CarregaDados()
        {
            return new BindingList<object>();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // ErrorProvider
            // 
            this.ErrorProvider.ContainerControl = this;
            // 
            // CustomForm
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(522, 279);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CustomForm";
            this.Text = "CustomForm";
            this.Load += new System.EventHandler(this.CustomForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
