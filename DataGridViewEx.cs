﻿using System.ComponentModel;

// ReSharper disable CheckNamespace
namespace WinFormsHelpers {
  // ReSharper restore CheckNamespace

  [Designer(typeof(System.Windows.Forms.Design.ControlDesigner))]
  public class DataGridViewEx : System.Windows.Forms.DataGridView {
  }
}
