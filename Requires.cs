﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace WinFormsHelpers {
  public static class Requires {

    static readonly Dictionary<Expression<Func<bool>>, Func<bool>> Cache
      = new Dictionary<Expression<Func<bool>>, Func<bool>>();

    /// <summary>
    /// Compiles the given boolean expression and executes the resulting delegate. 
    /// If returns False, throws an ArgumentNullException with the correct parameter name
    /// </summary>
    /// <param name="verifiableExpresson"></param>
    public static void That(Expression<Func<bool>> verifiableExpresson) {
      var binaryExpression = (BinaryExpression)verifiableExpresson.Body;
      var memberExpression = (MemberExpression)binaryExpression.Left;
      var paramName = memberExpression.Member.Name;

      Func<bool> @delegate;
      if (!Cache.TryGetValue(verifiableExpresson, out @delegate)) {
        @delegate = verifiableExpresson.Compile();
        Cache[verifiableExpresson] = @delegate;
      }

      if (!@delegate.Invoke())
        throw new ArgumentNullException(paramName);
    }
  }
}