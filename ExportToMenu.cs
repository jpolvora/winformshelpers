﻿using System;

namespace WinFormsHelpers {
  [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
  public class ExportToMenu : Attribute {
    public string Titulo { get; private set; }
    public string Grupo { get; private set; }
    public int Ordem { get; private set; }

    public ExportToMenu(string titulo, string grupo, int ordem) {
      Ordem = ordem;
      Titulo = titulo;
      Grupo = grupo;
    }
  }
}