﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;

namespace WinFormsHelpers
{
    public static class EntityExtensions
    {
        public static IEnumerable<Tuple<string, string>> GetErrors(this DbEntityValidationException ex)
        {
            foreach (var validationResult in ex.EntityValidationErrors)
            {
                foreach (var error in validationResult.ValidationErrors)
                {
                    yield return Tuple.Create(error.PropertyName, error.ErrorMessage);
                }
            }
        }

        public static void TrataErro(this DbEntityValidationException ex, CustomForm form)
        {
            BindingManagerBase manager = form.BindingSrc.CurrencyManager;
            foreach (var tuple in GetErrors(ex))
            {
                foreach (Binding binding in manager.Bindings)
                {
                    if (binding.BindingMemberInfo.BindingField == tuple.Item1)
                    {
                        if (binding.Control != null)
                            form.ErrorProvider.SetError(binding.Control, tuple.Item2);
                    }
                }
            }
        }

        public static int GetMax<T>(this DbContext ctx, Expression<Func<T, int>> expression) where T : class
        {
            var lastId = 0;
            var set = ctx.Set<T>();
            if (set.Any())
            {
                lastId = set.Max(expression);
            }

            return lastId + 1;
        }


        public static void SetMax<T>(this DbContext ctx, T instance, Expression<Func<T, int>> expression) where T : class
        {
            var max = GetMax(ctx, expression);

            var propertyInfo = expression.GetMemberInfo() as PropertyInfo;
            if (propertyInfo != null)
            {
                propertyInfo.SetValue(instance, max, null);
            }
        }

        public static void GravaFormulario(this DbContext ctx, CustomForm form, bool forceRefresh = false)
        {
            form.ErrorProvider.Clear();
            const string msgErro = "Existem dados inválidos. Corrija antes de gravar";
            var bindingSrc = form.BindingSrc;
            if (bindingSrc.Current == null)
            {
                "Nada há dados a serem salvos!".Alerta();
                return;
            }
            bindingSrc.EndEdit();
            try
            {
                var result = form.ValidateChildren(ValidationConstraints.Enabled);
                if (!result)
                {
                    msgErro.Alerta();
                    return;
                }

                var entry = ctx.Entry(bindingSrc.Current);

                if (entry.State == EntityState.Detached)
                {
                    if (form.Status == CustomForm.StatusFormulario.Inserindo)
                    {
                        entry.State = EntityState.Added;
                    }
                    else if (form.Status == CustomForm.StatusFormulario.Editando)
                    {
                        entry.State = EntityState.Modified;
                    }
                }
                var changes = ctx.SaveChanges();
                if (changes > 0)
                    "Dados gravados com sucesso!".Alerta();

                form.Status = CustomForm.StatusFormulario.Navegando;
                if (forceRefresh)
                    bindingSrc.Atualiza();
                form.BindingSrc.ResetCurrentItem();
            }
            catch (DbEntityValidationException ex)
            {
                ex.TrataErro(form);
                msgErro.Alerta();
            }
            catch (Exception ex)
            {
                "Ocorreu um erro: {0}".Fmt(ex.LoopException()).Alerta(5000);
            }
        }

        public static void ConfirmaExclusao(this DbContext ctx, CustomForm form)
        {
            var currentItem = form.BindingSrc.Current;
            if (currentItem == null)
                return;

            if ("Confirma exclusão deste registro {0} ?".Fmt(currentItem).Confirma())
            {
                form.BindingSrc.RemoveCurrent();
                try
                {
                    ctx.SaveChanges();
                    if (form.BindingSrc.Count == 0)
                        form.BindingSrc.Atualiza();
                }
                catch (Exception ex)
                {
                    ex.LoopException().Alerta(5000);
                }
            }
        }
    }
}