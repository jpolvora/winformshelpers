﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace WinFormsHelpers
{
    public static class ValidatorExtensions
    {
        static readonly Dictionary<Expression, Delegate> CompiledExpressions = new Dictionary<Expression, Delegate>();

        public static ValidationRule<T> GetRequiredValidator<T>(this BindBuilder<T> bindBuilder, string dataMember) where T : class
        {
            var expression = CSharpExtensions.GenerateMemberExpression<T, object>(dataMember);
            return GetRequiredValidator(expression);
        }


        public static ValidationRule<T> GetRequiredValidator<T>(this Expression<Func<T, object>> dataMember)
          where T : class
        {
            Requires.That(() => dataMember != null);

            var propertyInfo = (PropertyInfo)dataMember.GetMemberInfo();
            var @default = propertyInfo.PropertyType.GetDefaultValueForType();

            Delegate compiledExpression;
            if (!CompiledExpressions.TryGetValue(dataMember, out compiledExpression))
            {
                compiledExpression = dataMember.Compile();
                CompiledExpressions[dataMember] = compiledExpression;
            }

            Func<T, bool> wrapper = x =>
            {
                var lhr = compiledExpression.DynamicInvoke(x);
                return !Equals(lhr, @default);
            };

            return new ValidationRule<T>(wrapper, "Campo obrigatório");
        }

        public static bool IsCnpj(this string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
                return false;

            var multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            int soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto;
            return cnpj.EndsWith(digito);
        }

        public static bool IsCpf(this string cpf)
        {
            if (string.IsNullOrEmpty(cpf))
                return false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto;
            return cpf.EndsWith(digito);
        }
    }
}