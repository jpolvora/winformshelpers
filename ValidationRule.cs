﻿using System;
using System.Reflection;

namespace WinFormsHelpers
{
    public class ValidationRule<T> where T : class
    {
        private readonly Func<T, bool> _executor;
        public string ErrorMessage { get; private set; }

        public ValidationRule(Func<T, bool> executor, string errorMessage)
        {
            ErrorMessage = errorMessage;
            _executor = executor;
        }

        public bool IsValid(T instance)
        {
            var result = _executor(instance);
            return result;
        }
    }

    public class RequiredValidationRule<T> : ValidationRule<T> where T : class
    {
        private static PropertyInfo _info;

        public RequiredValidationRule(PropertyInfo info)
            : base(FunctionRequired, "Campo Obrigatório!")
        {
            _info = info;
        }

        static bool FunctionRequired(T instance)
        {
            var getValue = _info.GetValue(instance, null);
            return getValue != null;
        }
    }
}