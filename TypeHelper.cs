﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace WinFormsHelpers {
  public static class TypeHelper {
    static readonly Dictionary<string, Type> TypeCache
      = new Dictionary<string, Type>();

    public static void Initialize(Assembly[] assemblies) {
      AddAssemblyTypes(assemblies);
    }

    static void AddAssemblyTypes(IEnumerable<Assembly> assemblies) {
      foreach (var assembly in assemblies) {
        foreach (var type in assembly.GetTypes()) {
          TypeCache[type.Name] = type;
        }
      }
    }

    static TypeHelper() {
      var thisAssembly = typeof(TypeHelper).Assembly;

      var assemblies = new[]
      {
        thisAssembly,
        Assembly.GetEntryAssembly(), 
        //Assembly.GetCallingAssembly(), 
        //Assembly.GetExecutingAssembly()
      };

      AddAssemblyTypes(assemblies);
    }

    public static IEnumerable<Type> FindTypes<TBaseType>() {
      var baseType = typeof(TBaseType);
      return TypeCache.Values.Where(baseType.IsAssignableFrom);
    }

    public static Type FindType<TBaseType>() {
      var baseType = typeof(TBaseType);
      return TypeCache.Values.FirstOrDefault(baseType.IsAssignableFrom);
    }

    public static Type GetType(string typeName) {
      return TypeCache.ContainsKey(typeName)
        ? TypeCache[typeName]
        : null;
    }

    public static object TryActivate(this Type type) {
      try {
        return Activator.CreateInstance(type);
      } catch (Exception) {
        return null;
      }
    }

    public static T TryActivate<T>(string typeName) where T : class {
      var type = GetType(typeName);
      return TryActivate<T>(type);
    }

    public static T TryActivate<T>(this Type type) where T : class {
      if (type != null) {
        return Activator.CreateInstance(type) as T;
      }
      return default(T);
    }

    public static T Activate<T>() where T : class, new() {
      return new T();
    }
  }
}