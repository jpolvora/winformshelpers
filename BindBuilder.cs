﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;


namespace WinFormsHelpers
{
    public abstract class BindBuilder
    {
        static BindBuilder()
        {
            BindableProperties.Add(typeof(Form), "Text");
            BindableProperties.Add(typeof(Button), "Text");
            BindableProperties.Add(typeof(TextBox), "Text");
            BindableProperties.Add(typeof(CheckBox), "Checked");
            BindableProperties.Add(typeof(ComboBox), "SelectedValue");
            BindableProperties.Add(typeof(DateTimePicker), "Value");
            BindableProperties.Add(typeof(MaskedTextBox), "Text");
            BindableProperties.Add(typeof(LinkLabel), "Text");
        }

        public static readonly Dictionary<Type, string> BindableProperties = new Dictionary<Type, string>();
    }

    public class BindBuilder<T> : BindBuilder where T : class
    {
        private readonly CustomForm _form;
        static BindBuilder()
        {
            var modelProperties = typeof(T).GetProperties();
            foreach (var modelProperty in modelProperties)
            {
                PropertyInfos[modelProperty.Name.ToUpperInvariant()] = modelProperty;
            }
        }
        // ReSharper disable once StaticFieldInGenericType
        static readonly Dictionary<string, PropertyInfo> PropertyInfos = new Dictionary<string, PropertyInfo>();
        // ReSharper disable once StaticFieldInGenericType
        static readonly Dictionary<string, Binding> Bindings = new Dictionary<string, Binding>();

        public BindBuilder(CustomForm form)
        {
            _form = form;
            if (_form.BindingSrc.DataSource == null)
                _form.BindingSrc.DataSource = typeof(T);
        }

        public BindBuilder<T> BindAll()
        {
            foreach (var control in _form.GetChild<Control>())
            {
                var dataMember = ParseTag(control);
                if (string.IsNullOrEmpty(dataMember))
                    continue;

                if (dataMember.Contains("*"))
                {
                    var expression = CSharpExtensions.GenerateMemberExpression<T, object>(dataMember.Replace("*", ""));
                    var label = FindLabelForControl(control.Name);
                    Bind(control, expression, true, label);
                }
                else Bind(control, dataMember.Replace("*", ""));

                ApplyAttributes(control, dataMember);

            }
            return this;
        }

        private static void ApplyAttributes(IDisposable component, string dataMember)
        {
            var txb = component as TextBox;

            if (txb != null)
            {
                var upper = dataMember.Replace("*", "").ToUpperInvariant();
                if (PropertyInfos.ContainsKey(upper))
                {
                    var pi = PropertyInfos[upper];
                    if (pi != null)
                    {
                        var strLenAttr = pi.GetCustomAttributes(true).OfType<StringLengthAttribute>().FirstOrDefault();
                        if (strLenAttr != null)
                        {
                            if (strLenAttr.MaximumLength > 0)
                            {
                                txb.MaxLength = strLenAttr.MaximumLength;
                            }
                        }
                        //var reqAttr = pi.GetCustomAttribute<RequiredAttribute>();
                        //if (reqAttr != null)
                        //{

                        //}
                    }
                }
            }
        }

        public BindBuilder<T> Bind(IBindableComponent component, Expression<Func<T, object>> dataMember, bool obrigatorio, Label lbl)
        {
            if (!obrigatorio) return Bind(component, dataMember);

            var validationRule = dataMember.GetRequiredValidator();
            if (lbl != null)
            {
                lbl.Text += "*";
                lbl.Font = new Font(lbl.Font, FontStyle.Bold);
            }
            return Bind(component, dataMember, validationRule);
        }

        public BindBuilder<T> Bind(IBindableComponent component, Expression<Func<T, object>> dataMember, Func<T, bool> validation, string errorMessage, Action<Binding> action = null)
        {
            var validationRule = new ValidationRule<T>(validation, errorMessage);
            return Bind(component, dataMember, validationRule, action);
        }

        public BindBuilder<T> Bind(IBindableComponent component, Expression<Func<T, object>> dataMember, ValidationRule<T> validationRule = null, Action<Binding> action = null)
        {
            var member = dataMember.GetFullSortName();
            var binding = Bind(component, member);

            if (action != null)
            {
                action(binding);
            }

            if (binding == null || validationRule == null) return this;
            return IncludeValidation(component as Control, validationRule);
        }

        public BindBuilder<T> AddValidationFor(Expression<Func<T, object>> expression, Func<T, bool> validationFunc, string msg)
        {
            var validationRule = new ValidationRule<T>(validationFunc, msg);
            return AddValidationFor(expression, validationRule);
        }

        public BindBuilder<T> AddValidationFor(Expression<Func<T, object>> expression, ValidationRule<T> validationRule)
        {
            var datamember = expression.GetFullSortName();
            if (Bindings.ContainsKey(datamember))
            {
                var binding = Bindings[datamember];
                IncludeValidation(binding.Control, validationRule);
            }
            return this;
        }

        public BindBuilder<T> ForControl(Control control, ValidationRule<T> validationRule)
        {
            var datamember = ParseTag(control);
            if (string.IsNullOrEmpty(datamember))
                return this;

            if (Bindings.ContainsKey(datamember))
            {
                var binding = Bindings[datamember];
                IncludeValidation(binding.Control, validationRule);
            }
            return this;
        }

        private Binding Bind(IBindableComponent component, string dataMember)
        {
            var bindableProperty = GetBindableProperty(component.GetType());
            if (string.IsNullOrEmpty(bindableProperty))
                throw new Exception(string.Format("Não há uma propriedade configurada para este tipo de componente: {0}", component));

            var oldBinding = component.DataBindings[bindableProperty];
            if (oldBinding != null)
            {
                //componente já possui binding
                return oldBinding;
                //component.DataBindings.Remove(oldBinding);
            }

            var binding = new Binding(bindableProperty, _form.BindingSrc, dataMember, true)
            {
                ControlUpdateMode = ControlUpdateMode.OnPropertyChanged,
                DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
            };

            try
            {
                component.DataBindings.Add(binding);

                Bindings[dataMember] = binding;
            }
            catch (Exception ex)
            {
                ex.LoopException().Alerta();
            }
            return binding;
        }

        private BindBuilder<T> IncludeValidation(Control control, ValidationRule<T> validationRule)
        {
            if (control == null) return this;
            control.Tag = validationRule;
            control.Validating += ControlOnValidating;
            control.Validated += ControlOnValidated;
            control.Disposed += ControlOnDisposed;
            return this;
        }

        private Label FindLabelForControl(string controlName)
        {
            if (controlName.Length <= 3) return null;

            var labelName = "lbl" + controlName.Substring(3);
            var label = _form.GetChild<Label>().FirstOrDefault(x => x.Name == labelName);
            return label;
        }


        public static string GetBindableProperty(Type getType)
        {
            return BindableProperties.ContainsKey(getType)
              ? BindableProperties[getType]
              : string.Empty;
        }

        static string ParseTag(Control control)
        {
            if (control == null)
                return null;

            if (string.IsNullOrEmpty(GetBindableProperty(control.GetType())))
                return null;

            if (control.Tag == null) return null;

            var dataMember = control.Tag.ToString();
            return PropertyInfos.ContainsKey(dataMember.Replace("*", "").ToUpperInvariant())
              ? dataMember
              : null;
        }

        private static void ControlOnDisposed(object sender, EventArgs eventArgs)
        {
            var control = (Control)sender;
            control.Validating -= ControlOnValidating;
            control.Validated -= ControlOnValidated;
            control.Disposed -= ControlOnDisposed;
        }

        private static void ControlOnValidating(object sender, CancelEventArgs cancelEventArgs)
        {
            var control = (Control)sender;
            var defaultProperty = GetBindableProperty(control.GetType());
            if (string.IsNullOrEmpty(defaultProperty)) return;
            var binding = control.DataBindings[defaultProperty];
            if (binding == null) return;

            var bs = binding.DataSource as BindingSource;
            if (bs == null)
                return;
            var instance = bs.Current as T;
            if (instance == null)
                return;

            var validationRule = (ValidationRule<T>)control.Tag;

            var result = validationRule.IsValid(instance);
            if (result) return;

            cancelEventArgs.Cancel = true;
            var form = control.FindForm() as CustomForm;
            if (form != null)
            {
                form.ErrorProvider.SetError(control, validationRule.ErrorMessage);
            }
        }

        private static void ControlOnValidated(object sender, EventArgs eventArgs)
        {
            var control = (Control)sender;
            var form = control.FindForm() as CustomForm;
            if (form == null) return;
            form.ErrorProvider.SetError(control, string.Empty);
        }
    }
}