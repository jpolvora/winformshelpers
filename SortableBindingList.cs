﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace WinFormsHelpers {
  public static class SortableBindingList {
    public static SortableBindingList<T> Get<T>(T item) {
      return ToBindingList(new[] { item });
    }

    public static SortableBindingList<T> ToBindingList<T>(this IList<T> items) {
      return new SortableBindingList<T>(items);
    }
  }

  public class SortableBindingList<T> : BindingList<T> {
    private PropertyDescriptor _propertyDescriptor;
    private ListSortDirection _listSortDirection;
    private bool _isSorted;

    protected override bool SupportsSortingCore {
      get {
        return true;
      }
    }

    protected override bool IsSortedCore {
      get {
        return _isSorted;
      }
    }

    protected override PropertyDescriptor SortPropertyCore {
      get {
        return _propertyDescriptor;
      }
    }

    protected override ListSortDirection SortDirectionCore {
      get {
        return _listSortDirection;
      }
    }

    public SortableBindingList() {
    }

    public SortableBindingList(IEnumerable<T> list)
      : this() {
      Items.Clear();
      foreach (var obj in list)
        Add(obj);
    }

    protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction) {
      var list = Items as List<T>;
      _propertyDescriptor = prop;
      _listSortDirection = direction;
      _isSorted = true;
      list.Sort(GetComparisonDelegate(prop, direction));
    }

    protected override void RemoveSortCore() {
      _isSorted = false;
      _propertyDescriptor = base.SortPropertyCore;
      _listSortDirection = base.SortDirectionCore;
    }

    protected virtual Comparison<T> GetComparisonDelegate(PropertyDescriptor propertyDescriptor, ListSortDirection direction) {
      return (t1, t2) => (direction == ListSortDirection.Ascending ? 1 : -1) *
        (propertyDescriptor.PropertyType != typeof(string)
        ? Comparer.Default.Compare(propertyDescriptor.GetValue(t1), propertyDescriptor.GetValue(t2))
        : StringComparer.CurrentCulture.Compare(propertyDescriptor.GetValue(t1), propertyDescriptor.GetValue(t2)));
    }
  }
}