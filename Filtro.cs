﻿using System;
using System.Linq.Expressions;

namespace WinFormsHelpers {
  public class Filtro<T> {
    public Filtro(string texto, Expression<Func<T, bool>> @where) {
      Texto = texto;
      Where = @where;
    }

    public string Texto { get; private set; }
    public Expression<Func<T, bool>> Where { get; private set; }
  }
}