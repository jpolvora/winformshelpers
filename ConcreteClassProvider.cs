﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsHelpers {
  public class ConcreteClassProvider : TypeDescriptionProvider {
    public ConcreteClassProvider()
      : base(TypeDescriptor.GetProvider(typeof(Form))) {
    }

    public override Type GetReflectionType(Type objectType, object instance) {
      var resultingType = GetConcreteType(objectType);

      return resultingType;
    }

    private static Type GetConcreteType(Type objectType) {
      while (true) {
        if (!objectType.IsAbstract || !objectType.ContainsGenericParameters)
          return objectType;

        var attribute = objectType.GetCustomAttribute<ConcreteClassAttribute>();

        if (attribute == null) return objectType;
        objectType = attribute.ConcreteType;
      }
    }

    public override object CreateInstance(IServiceProvider provider, Type objectType, Type[] argTypes, object[] args) {
      var resultingType = GetConcreteType(objectType);
      return base.CreateInstance(provider, resultingType, argTypes, args);
    }
  }
}
