﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace WinFormsHelpers
{
    public static class CSharpExtensions
    {
        public static bool IsNotNull(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

        [DebuggerStepThrough]
        public static T Cast<T>(this object o) where T : class
        {
            return o as T;
        }

        public static object GetDefaultValueForType(this Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        public static string LoopException(this Exception ex, bool writelog = true)
        {
            var sb = new StringBuilder();
            sb.AppendLine(ex.ToString());
            if (ex.InnerException != null)
            {
                sb.AppendLine();
                sb.Append(ex.InnerException.LoopException());
            }
            var log = sb.ToString();
            if (writelog)
                ExceptionLogger.Instance.Log(log);
            return log;
        }

        public static string GetFullSortName<TModel, TProperty>(this Expression<Func<TModel, TProperty>> expression)
        {
            var memberNames = new List<string>();

            var memberExpression = GetMemberExpression(expression);

            while (null != memberExpression)
            {
                memberNames.Add(memberExpression.Member.Name);
                memberExpression = memberExpression.Expression as MemberExpression;
            }

            memberNames.Reverse();
            string fullName = string.Join(".", memberNames.ToArray());
            return fullName;
        }

        public static string Fmt(this string str, params object[] args)
        {
            return string.Format(str, args);
        }

        public static MemberExpression GetMemberExpression(this Expression expression)
        {
            var lambda = (LambdaExpression)expression;

            MemberExpression memberExpression;
            var body = lambda.Body as UnaryExpression;
            if (body != null)
            {
                var unaryExpression = body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)lambda.Body;
            }

            return memberExpression;
        }

        public static MemberInfo GetMemberInfo(this Expression expression)
        {

            return GetMemberExpression(expression).Member;
        }

        public static Expression<Func<TModel, T>> GenerateMemberExpression<TModel, T>(string propertyName) where TModel : class
        {
            var propertyInfo = typeof(TModel).GetProperty(propertyName);

            var x = Expression.Parameter(typeof(TModel), "x");
            Expression columnExpr = Expression.Property(x, propertyInfo);

            if (propertyInfo.PropertyType != typeof(T))
                columnExpr = Expression.Convert(columnExpr, typeof(T));

            return Expression.Lambda<Func<TModel, T>>(columnExpr, x);
        }
    }
}