﻿using System;

namespace WinFormsHelpers {
  public class PropertyValidation<TBindingModel> {
    private Func<TBindingModel, bool> _validationCriteria;
    private string _errorMessage;
    private readonly string _propertyName;

    public PropertyValidation(string propertyName) {
      this._propertyName = propertyName;
    }

    public PropertyValidation<TBindingModel> WithMessage(string errorMessage) {
      if (this._errorMessage != null) {
        throw new InvalidOperationException("You can only set the message once.");
      }

      this._errorMessage = errorMessage;
      return this;
    }

    public PropertyValidation<TBindingModel> When(Func<TBindingModel, bool> validationCriteria) {
      if (this._validationCriteria != null) {
        throw new InvalidOperationException("You can only set the validation criteria once.");
      }

      this._validationCriteria = validationCriteria;
      return this;
    }

    public bool IsInvalid(TBindingModel presentationModel) {
      if (_validationCriteria == null) {
        throw new InvalidOperationException("No criteria have been provided for this validation. (Use the 'When(..)' method.)");
      }

      return _validationCriteria(presentationModel);
    }

    public string GetErrorMessage() {
      if (_errorMessage == null) {
        throw new InvalidOperationException("No error message has been set for this validation. (Use the 'WithMessage(..)' method.)");
      }

      return _errorMessage;
    }

    public string PropertyName {
      get { return _propertyName; }
    }
  }
}