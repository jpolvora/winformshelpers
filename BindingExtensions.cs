﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace WinFormsHelpers
{
    public static class BindingExtensions
    {
        public static void Initialize()
        {
        }

        public static void StopBinding(this Binding b)
        {
            b.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            b.ControlUpdateMode = ControlUpdateMode.Never;
        }

        public static void ResumeBinding(this Binding b, bool propertyChanged = true)
        {
            b.DataSourceUpdateMode = propertyChanged ? DataSourceUpdateMode.OnPropertyChanged : DataSourceUpdateMode.OnValidation;
            b.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged;
        }

        public static void Bind<T>(this Control control, T dataSource, Expression<Func<T, object>> member) where T : class
        {
            var propertyName = BindBuilder<T>.GetBindableProperty(control.GetType());
            var text = member.GetFullSortName();
            var binding = new Binding(propertyName, dataSource, text);
            control.DataBindings.Add(binding);
        }

        public static BindBuilder<T> ForModel<T>(this CustomForm form)
          where T : class
        {
            return new BindBuilder<T>(form);
        }

        public static bool BindComboBox<T>(this ComboBox cmb, IEnumerable<T> values, Expression<Func<T, string>> keySelector, Expression<Func<T, object>> valueSelector, bool force = false)
        {
            Requires.That(() => cmb != null);

            cmb.DisplayMember = keySelector.GetMemberInfo().Name;
            cmb.ValueMember = valueSelector.GetFullSortName();

            if (!force)
            {
                if (Equals(cmb.DataSource, values))
                    return false;

                var source = cmb.DataSource as BindingSource;
                if (source != null)
                {
                    if (Equals(source.DataSource, values))
                        return false;
                }
            }
            else
            {
                cmb.DataSource = null;
            }

            var bs = new BindingSource(values, null);
            cmb.DataSource = bs;

            return true;
        }

        public static void BindKeyValuePair<TKey, TValue>(this ComboBox cmb, IEnumerable<KeyValuePair<TKey, TValue>> values)
        {
            Requires.That(() => cmb != null);
            cmb.DataSource = null;

            cmb.DisplayMember = "Key";
            cmb.ValueMember = "Value";
            cmb.DataSource = new BindingSource(values, null);
        }


        public static void BindEnum<T>(this ComboBox cmb)
        {
            var values = Enumerable.Cast<int>(Enum.GetValues(typeof(T)))
                .Select(s => new KeyValuePair<string, T>(Enum.GetName(typeof(T), s), (T)Enum.ToObject(typeof(T), s)));

            cmb.BindKeyValuePair(values);
        }

        public static GridBinder<T> Configure<T>(this DataGridView dgv, CustomForm form)
            where T: class
        {
            Requires.That(() => dgv != null);
            Requires.That(() => form != null);

            dgv.AutoGenerateColumns = false;
            return new GridBinder<T>(dgv, form);
        }
    }
}