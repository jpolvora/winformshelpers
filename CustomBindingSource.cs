﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace WinFormsHelpers
{
    [Serializable]
    public class CustomBindingSource : BindingSource
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Func<IBindingList> CarregarDados { get; set; }
        public event EventHandler BeforeUpdate = delegate { };
        public event EventHandler AfterUpdate = delegate { };

        public void Atualiza()
        {
            try
            {
                var current = this.Current;
                BeforeUpdate(this, EventArgs.Empty);
                this.DataSource = CarregarDados();
                AfterUpdate(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                "Erro ao carregar dados: {0}{1}".Fmt(Environment.NewLine, ex.LoopException()).Alerta(3000);
            }
        }

        public bool CanInsertOrEdit { get; set; }

        public CustomBindingSource(IContainer container)
            : base(container)
        {
        }

        public CustomBindingSource(object dataSource, string dataMember)
            : base(dataSource, dataMember)
        {
        }

        public override bool AllowEdit
        {
            get { return CanInsertOrEdit && base.AllowEdit; }
        }
    }
}