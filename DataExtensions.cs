﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using FastMember;

namespace WinFormsHelpers
{
    public static class DataExtensions
    {
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string ordering, bool descending = false, params object[] values)
        {
            var type = typeof(T);
            var property = type.GetProperty(ordering);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExp = Expression.Lambda(propertyAccess, parameter);
            string method = descending ? "OrderByDescending" : "OrderBy";
            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), method, new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExp));
            return source.Provider.CreateQuery<T>(resultExp);
        }

        public static IQueryable<T> WhereStr<T>(this IQueryable<T> source, string fieldName, params object[] values)
        {
            var type = typeof(T);
            var property = type.GetProperty(fieldName);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var whereExpr = Expression.Lambda(propertyAccess, parameter);
            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), "Where", new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(whereExpr));
            return source.Provider.CreateQuery<T>(resultExp);
        }

        public static IEnumerable<KeyValuePair<TKey, TElement>> ToKeyValuePairs<TSource, TKey, TElement>(
          this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return source.Select(item => new KeyValuePair<TKey, TElement>(keySelector(item), elementSelector(item)));
        }

        public static IEnumerable<T> Filter<T>(this IEnumerable<T> list, Func<T, bool> filterParam)
        {
            return list.Where(filterParam);
        }

        public static DataTable GetDataTable<T>(this IEnumerable<T> dataSource)
        {
            var table = new DataTable();
            using (var reader = ObjectReader.Create(dataSource))
            {
                table.Load(reader);
            }
            return table;
        }

    }
}