﻿using System;
using System.Data.Entity;

namespace WinFormsHelpers {
  [Serializable]
  public class CustomBindingSource<TEntity> : CustomBindingSource
    where TEntity : class {

    public Func<DbContext> ContextGetter { get; private set; }

    public event EventHandler BeforeUpdate = delegate { };
    public event EventHandler AfterUpdate = delegate { };

    public TEntity CurrentEntity {
      get {
        return Current as TEntity;
      }
    }

    public CustomBindingSource(Func<DbContext> contextGetter) {
      ContextGetter = contextGetter;
    }

    public override void Atualiza() {
      try {
        OnBeforeUpdate();
        BeforeUpdate(this, EventArgs.Empty);
        this.DataSource = DataLoader();
        OnAfterUpdate();
        AfterUpdate(this, EventArgs.Empty);
      } catch (Exception ex) {
        "Erro ao carregar dados: {0}{1}".Fmt(Environment.NewLine, ex.LoopException()).Alerta(3000);
      }
    }

    protected virtual void OnBeforeUpdate() {
    }

    protected virtual void OnAfterUpdate() {
    }
  }
}