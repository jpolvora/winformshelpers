﻿using System;
using System.Diagnostics;

namespace WinFormsHelpers {
  public class ExceptionLogger {
    private static ExceptionLogger _instance = new ExceptionLogger();

    public static ExceptionLogger Instance {
      get {
        return _instance;
      }
    }

    public static Action<string> WriteLog = s => { };

    public void Log(string exception) {
      WriteLog(exception);
    }
  }
}