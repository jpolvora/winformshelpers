﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mime;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastMember;
using Hjerpbakk.AsyncMethodCaller;

namespace WinFormsHelpers
{
    public static class ControlExtensions
    {

        public static bool IsInDesignMode(this Control control)
        {
            return ResolveDesignMode(control);
        }

        private static bool ResolveDesignMode(Control control)
        {
            // Get the protected property
            var designModeProperty = control.GetType().GetProperty("DesignMode", BindingFlags.Instance | BindingFlags.NonPublic);

            // Get the controls DesignMode value
            var designMode = (bool)designModeProperty.GetValue(control, null);

            // Test the parent if it exists
            if (control.Parent != null)
            {
                designMode |= ResolveDesignMode(control.Parent);
            }

            return designMode;
        }

        public static void InvokeOnUI(this Control control, Action action)
        {
            if (control.InvokeRequired)
                control.Invoke(action);
            else
                action();
        }

        public static void FillMenu(this MenuStrip menu)
        {
            var forms = TypeHelper.FindTypes<Form>();

            Dictionary<Type, ExportToMenu> attrs = new Dictionary<Type, ExportToMenu>();
            foreach (var f in forms)
            {
                var att = f.GetCustomAttributes(typeof(ExportToMenu), true);
                if (att.Length == 0)
                    continue;
                attrs.Add(f, att[0].Cast<ExportToMenu>());
            }

            foreach (var type in attrs.OrderBy(x => x.Value.Grupo))
            {
                var attribute = type.Value;

                var grupo = menu.Items
                  .OfType<ToolStripMenuItem>()
                  .FirstOrDefault(x => x.Text == attribute.Grupo)
                  ?? new ToolStripMenuItem(attribute.Grupo);

                menu.Items.Add(grupo);

                var menuItem = new ToolStripMenuItem(attribute.Titulo)
                {
                    Tag = type.Key
                };
                menuItem.Click += (sender, args) =>
                {
                    sender.Cast<ToolStripMenuItem>().OpenForm(menu.FindForm());
                };
                grupo.DropDownItems.Add(menuItem);
            }
        }

        public static void OpenForm(this ToolStripMenuItem menuItem, Form mainForm)
        {
            if (menuItem == null)
                return;

            var tag = menuItem.Tag;
            if (tag == null)
                throw new InvalidOperationException("Controle deve ter a Tag com o nome do formulário ou um objeto Type");

            mainForm.InvokeOnUI(() =>
            {
                try
                {
                    var formInstance = tag is Type
                      ? (tag as Type).TryActivate<Form>()
                      : TypeHelper.TryActivate<Form>(tag.ToString());

                    if (formInstance != null)
                    {
                        formInstance.Owner = mainForm;
                        formInstance.ShowDialog();
                    }
                    else
                    {
                        "Classe '{0}' não encontrada".Fmt(tag).Alerta();
                    }
                }
                catch (Exception ex)
                {
                    ex.LoopException().Alerta();
                }
            });
        }

        public static T CreateAndShow<T>(this Form form, FormClosedEventHandler callBack, bool modal) where T : Form, new()
        {
            var formToShow = new T();
            if (modal)
                formToShow.ShowDialog(form);
            else formToShow.Show(form);
            if (callBack != null)
                formToShow.FormClosed += callBack;

            return formToShow;
        }

        public static T CreateAndShow<T>(this Form form, FormClosedEventHandler callBack = null) where T : Form, new()
        {
            return form.CreateAndShow<T>(callBack, false);
        }

        public static T CreateAndShowModal<T>(this Form form, FormClosedEventHandler callBack = null) where T : Form, new()
        {
            return form.CreateAndShow<T>(callBack, true);
        }

        public static void Alerta(this string mensagem, int duracao = 2000, Color? corFonte = null)
        {
            var ft = new Font("Arial", 10, FontStyle.Bold);

            var msg = new Label
            {
                Text = string.IsNullOrEmpty(mensagem) ? "Concluido com Sucesso." : mensagem,
                TextAlign = ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
                Font = ft,
                ForeColor = corFonte.HasValue ? corFonte.Value : Color.Black
            };

            var sucesso = new Form
            {
                Width = 400,
                Height = 350,
                Text = "Alerta do Sistema",
                WindowState = FormWindowState.Normal,
                StartPosition = FormStartPosition.CenterScreen,
                ControlBox = false,
                BackColor = Color.White,
                TopMost = true,
                ShowInTaskbar = false
            };

            sucesso.Controls.Add(msg);
            sucesso.Show();

            var mc = new AsyncMethodCaller();
            mc.CallMethodAndContinue(() =>
            {
                Thread.Sleep(duracao);
            },
            () =>
            {
                sucesso.InvokeOnUI(() =>
                {
                    sucesso.Close();
                    sucesso.Dispose();
                });

            }, exception =>
            {

            });
        }



        public static bool Confirma(this string mensagem)
        {
            var dialog = MessageBox.Show(mensagem, "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
              MessageBoxDefaultButton.Button2);

            return dialog == DialogResult.Yes;
        }

        public static IEnumerable<Control> GetChild(this Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                yield return control;
                foreach (var child in control.GetChild())
                {
                    yield return child;
                }
            }
        }

        public static IEnumerable<T> GetChild<T>(this Control parent) where T : IBindableComponent
        {
            return parent.GetChild().OfType<T>();
        }

        public static IEnumerable<Control> GetParent(this Control child)
        {
            while (true)
            {
                if (child.Parent != null)
                {
                    var parent = child.Parent;
                    yield return parent;
                    child = parent;
                    continue;
                }
                break;
            }
        }

        public static IEnumerable<T> GetParent<T>(this Control child) where T : Control
        {
            return child.GetParent().OfType<T>();
        }

        public static void ApplyCpf(this TextBox textBox)
        {
            var binding = textBox.DataBindings["Text"];
            if (binding != null) binding.FormattingEnabled = true;
        }

        public static void SetEnabled(this Control parent, bool isEnabled)
        {
            foreach (var control in parent.GetChild<Control>())
            {
                control.Enabled = isEnabled;
            }
        }

        public static void SetColors(this Control parent, Color? backColorToUse = null, Color? foreColorToUse = null)
        {
            var backColor = backColorToUse.HasValue ? backColorToUse.Value : Color.White;
            var foreColor = foreColorToUse.HasValue ? foreColorToUse.Value : Color.DarkBlue;

            parent.ForControl<TextBox>(x => x.BackColor = backColor);
            parent.ForControl<TextBox>(x => x.ForeColor = foreColor);
            parent.ForControl<MaskedTextBox>(x => x.BackColor = backColor);
            parent.ForControl<MaskedTextBox>(x => x.ForeColor = foreColor);
            parent.ForControl<CheckBox>(x => x.BackColor = backColor);
            parent.ForControl<CheckBox>(x => x.ForeColor = foreColor);
            parent.ForControl<ComboBox>(x => x.BackColor = backColor);
            parent.ForControl<ComboBox>(x => x.ForeColor = foreColor);
            parent.ForControl<DateTimePicker>(x => x.BackColor = backColor);
            parent.ForControl<DateTimePicker>(x => x.ForeColor = foreColor);
        }

        public static void DisableEditableControls(this Control parent)
        {
            parent.ForControl<TextBox>(x => x.ReadOnly = true);
            parent.ForControl<MaskedTextBox>(x => x.ReadOnly = true);
            parent.ForControl<CheckBox>(x => x.Enabled = false);
            parent.ForControl<ComboBox>(x => x.Enabled = false);
            parent.ForControl<DateTimePicker>(x => x.Enabled = false);
        }

        public static void EnableEditableControls(this Control parent)
        {
            parent.ForControl<TextBox>(x => x.ReadOnly = false);
            parent.ForControl<MaskedTextBox>(x => x.ReadOnly = false);
            parent.ForControl<CheckBox>(x => x.Enabled = true);
            parent.ForControl<ComboBox>(x => x.Enabled = true);
            parent.ForControl<DateTimePicker>(x => x.Enabled = true);
        }

        public static void ForControl<T>(this Control parent, Action<T> action) where T : Control
        {
            foreach (var control in parent.GetChild<T>())
            {
                action(control);
            }
        }

        public static void SetProperties(this Control parent, Type type, string propertyName, object value)
        {
            var accessor = TypeAccessor.Create(type, true);
            foreach (var control in parent.GetChild())
            {
                if (type.IsInstanceOfType(control))
                    accessor[control, propertyName] = value;
            }
        }

        public static void SetFocusEvents(this Form form)
        {
            var controls = form.GetChild();
            foreach (var control in controls)
            {
                control.GotFocus += ControlOnGotFocus;
                control.LostFocus += ControlOnLostFocus;
                control.Disposed += ControlOnDisposed;
            }
        }

        private static void ControlOnDisposed(object sender, EventArgs eventArgs)
        {
            var control = sender.Cast<Control>();
            if (control != null)
            {
                control.Disposed -= ControlOnDisposed;
                control.GotFocus -= ControlOnGotFocus;
                control.LostFocus -= ControlOnLostFocus;
            }
        }

        [DebuggerStepThrough()]
        private static void ControlOnLostFocus(object sender, EventArgs eventArgs)
        {
            sender.Cast<Control>().BackColor = Color.White;
        }

        [DebuggerStepThrough()]
        private static void ControlOnGotFocus(object sender, EventArgs eventArgs)
        {
            sender.Cast<Control>().BackColor = Color.LightYellow;
        }

        public static void CustomBehaviour(this DateTimePicker dtp, DateTimePickerFormat format)
        {
            EventHandler handler = (sender, args) =>
            {
                if (dtp.Checked)
                {
                    dtp.Format = format; //
                }
                else
                {
                    dtp.Format = DateTimePickerFormat.Custom;
                    dtp.CustomFormat = "      ";
                }
            };

            dtp.ValueChanged += handler;

            dtp.Format = DateTimePickerFormat.Custom;
            dtp.CustomFormat = " ";

            dtp.Checked = false;

            handler(dtp, EventArgs.Empty);
        }
    }
}