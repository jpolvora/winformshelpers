﻿using System;
using System.Windows.Forms;

namespace WinFormsHelpers {
  public class CustomTextBox : TextBox {
    public int? AsInt32 {
      get {
        int result;
        if (Int32.TryParse(Text, out result)) {
          return result;
        }
        Text = "0";
        return 0;
      }
    }

    public string AsString {
      get {
        return string.IsNullOrEmpty(Text)
          ? ""
          : Text;
      }
    }
  }
}