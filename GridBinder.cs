﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace WinFormsHelpers
{
    public class GridBinder<T> where T : class
    {
        private readonly DataGridView _dgv;
        private readonly CustomForm _form;

        readonly Dictionary<string, Action<T, DataGridViewCell>> _actions = new Dictionary<string, Action<T, DataGridViewCell>>();

        public GridBinder(DataGridView dgv, CustomForm form)
        {
            Requires.That(() => dgv != null);
            _dgv = dgv;
            _form = form;
            if (_form.BindingSrc.DataSource == null)
                _form.BindingSrc.DataSource = typeof(T);

            dgv.CellClick += dgv_CellClick;
            dgv.ColumnAdded += DgvOnColumnAdded;
        }

        private static void DgvOnColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            var funcIsVisible = e.Column.Tag as Func<bool>;
            if (funcIsVisible != null)
            {
                e.Column.Visible = funcIsVisible();
            }
        }

        void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
                return;

            var row = _dgv.Rows[e.RowIndex];
            var column = _dgv.Columns[e.ColumnIndex];
            var instance = row.DataBoundItem as T;
            var cell = row.Cells[e.ColumnIndex];
            var key = column.HeaderText;

            Action<T, DataGridViewCell> handler;
            if (_actions.TryGetValue(key, out handler))
            {
                handler(instance, cell);
            }
        }

        public GridBinder<T> Texto(Expression<Func<T, object>> fieldName, string titulo = null, int largura = -1, string mascara = null, bool visivel = true)
        {
            var campo = fieldName.GetFullSortName();

            var dgc = new DataGridViewTextBoxColumn
            {
                DataPropertyName = campo,
                HeaderText = titulo ?? campo,
                Visible = visivel
            };

            if (!string.IsNullOrEmpty(mascara))
                dgc.DefaultCellStyle.Format = mascara;

            if (largura >= 0)
            {
                dgc.Width = largura;
            }
            else
            {
                dgc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            _dgv.Columns.Add(dgc);

            return this;
        }

        public GridBinder<T> Combo(Expression<Func<T, object>> fieldName, string titulo = null, int largura = -1, bool visivel = true)
        {
            var campo = fieldName.GetFullSortName();

            var dgc = new DataGridViewComboBoxColumn
            {
                DataPropertyName = campo,
                HeaderText = titulo ?? campo,
                Visible = visivel
            };

            if (largura >= 0)
            {
                dgc.Width = largura;
            }
            else
            {
                dgc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            _dgv.Columns.Add(dgc);

            return this;
        }

        public GridBinder<T> CheckBox(Expression<Func<T, object>> fieldName, string titulo = null, int largura = -1, bool visivel = true, string NomeColuna = null)
        {
            var campo = fieldName.GetFullSortName();
            var dgc = new DataGridViewCheckBoxColumn
            {
                DataPropertyName = campo,
                HeaderText = titulo ?? campo,
                Visible = visivel
            };

            if (!string.IsNullOrEmpty(NomeColuna))
                dgc.Name = NomeColuna;

            if (largura >= 0)
            {
                dgc.Width = largura;
            }
            else
            {
                dgc.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            _dgv.Columns.Add(dgc);

            return this;
        }

        public GridBinder<T> ExtraColumn(string titulo, string content, Action<T, DataGridViewCell> handler, Func<bool> isVisible)
        {
            var dgc = new DataGridViewButtonColumn()
            {
                //DataPropertyName = campo,
                HeaderText = titulo,
                ReadOnly = false
            };

            dgc.UseColumnTextForButtonValue = true;
            dgc.Text = content;

            dgc.Tag = isVisible;
            _dgv.Columns.Add(dgc);

            _actions[titulo] = handler;
            return this;
        }

        public DataGridView Bind()
        {
            _dgv.DataSource = _form.BindingSrc;
            _dgv.Tag = this;
            return _dgv;
        }
    }
}