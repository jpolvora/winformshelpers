﻿using System;
using System.Data.Entity;

namespace WinFormsHelpers {
  public class EntityForm<TEntity> : CustomForm
    where TEntity : class {

    public new CustomBindingSource<TEntity> BindingSrc {
      get { return base.BindingSrc as CustomBindingSource<TEntity>; }
      set { base.BindingSrc = value; }
    }

    public EntityForm() {
      InitializeComponent();
    }

    protected override CustomBindingSource GetBindingSrc() {
      return new CustomBindingSource<TEntity>(DbContextGetter);
    }

    protected virtual DbContext DbContextGetter() {
      return TypeHelper.TryActivate<DbContext>();
    }

    private void InitializeComponent() {
      ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
      this.SuspendLayout();
      // 
      // EntityForm
      // 
      this.ClientSize = new System.Drawing.Size(519, 297);
      this.Name = "EntityForm";
      this.Text = "EntityForm";
      ((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
      this.ResumeLayout(false);

    }
  }
}
