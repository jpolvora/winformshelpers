﻿using System;

namespace WinFormsHelpers {
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
  public class ConcreteClassAttribute : Attribute {
    readonly Type _concreteType;

    public ConcreteClassAttribute(Type concreteType) {
      _concreteType = concreteType;
    }

    public Type ConcreteType { get { return _concreteType; } }
  }
}